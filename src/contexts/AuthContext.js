import { createContext } from 'react';

const AuthContext = createContext(null);

export const AuthProvider = AuthContext.Provider;

export default AuthContext;

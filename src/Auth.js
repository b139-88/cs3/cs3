import axios from 'axios';

import { API } from './config';

export const login = (user) => {
  const headers = {
    'content-type': 'application/json',
  };

  return axios.post(`${API}/users/login`, user, { headers })
    .then((res) => res)
    .catch((error) => error.response);
};

export const register = (user) => {
  const headers = {
    'content-type': 'application/json',
  };

  return axios.post(`${API}/users/register`, user, { headers })
    .then((res) => res)
    .catch((error) => error.response);
};

export const authenticate = (data, next) => {
  if (typeof window !== 'undefined') {
    localStorage.setItem('token', data.data.token);
    next();
  }
};

export const getUserDetails = (token) => {
  const headers = {
    'content-type': 'application/json',
    authorization: `Bearer ${token}`,
  };

  return axios.get(`${API}/users/details`, { headers })
    .then((res) => res)
    .catch((error) => error.response);
};

export const isAuthenticated = () => {
  if (typeof window === 'undefined') {
    return false;
  }
  if (localStorage.getItem('token')) {
    return localStorage.getItem('token');
  }
  return false;
};

export default isAuthenticated;

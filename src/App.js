/* eslint-disable no-underscore-dangle */
import './assets/css/App.css';
import {
  Routes, Route, Link,
} from 'react-router-dom';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { Container } from 'react-bootstrap';
import AppNavbar from './components/core/AppNavBar';
import ErrorPage404 from './pages/ErrorPage404';
import MyAccount from './pages/user/MyAccount';
import { AuthProvider } from './contexts/AuthContext';
import { getUserDetails } from './Auth';
import Footer from './components/core/Footer';
import Home from './pages/Home';
import Newsletter from './components/core/Newsletter';

function App() {
  const token = localStorage.getItem('token');

  const [user, setUser] = useState({
    id: null,
    firstName: null,
    lastName: null,
    email: null,
    isAdmin: null,
  });

  // function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      firstName: null,
      lastName: null,
      email: null,
      isAdmin: null,
    });
  };

  useEffect(() => {
    if (token != null) {
      getUserDetails(token)
        .then((res) => {
          if (typeof res.data.user._id !== 'undefined') {
            setUser({
              id: res.data.user._id,
              firstName: res.data.user.firstName,
              lastName: res.data.user.lastName,
              email: res.data.user.email,
              isAdmin: res.data.user.isAdmin,
            });
          } else {
            Swal.fire({
              title: 'Error occurred!',
              icon: 'error',
              text: 'An error occurred while trying to retrieve user details. Please try again.',
            });
          }
        });
    }
  }, [token]);

  return (
    <>
      {/* eslint-disable-next-line react/jsx-no-constructed-context-values */}
      <AuthProvider value={{ user, setUser, unsetUser }}>
        <>
          <AppNavbar />
          <div id="body">
            <Routes>
              <Route path="/" element={<Home />} />
              {user.isAdmin && <Route path="about" element={<About />} />}
              {user.id != null && <Route path="my-account" element={<MyAccount />} />}
              <Route path="/*" element={<ErrorPage404 />} />
            </Routes>
          </div>
          <footer>
            <Footer />
          </footer>

        </>

      </AuthProvider>
    </>
  );
}

function About() {
  return (
    <>
      <main>
        <h2>Who are we?</h2>
        <p>
          That feels like an existential question, dont you
          think?
        </p>
      </main>
      <nav>
        <Link to="/">Home</Link>
      </nav>
    </>
  );
}

export default App;

import Error from '../components/core/Error';

function ErrorPage() {
  const errorMessage = {
    title: '404 - Not Found',
    message: 'You got lost in the galaxy.',
  };

  return (
    <Error errorProps={errorMessage} />
  );
}

export default ErrorPage;

import {
  Tab, Nav, Row, Col,
} from 'react-bootstrap';
import Login from '../../components/user/Login';
import Register from '../../components/user/Register';
import '../../assets/css/auth.css';

function AuthPage(props) {
  // eslint-disable-next-line react/prop-types
  const { toggleShow } = props;
  return (
    <Tab.Container defaultActiveKey="login">
      <Row>
        <Col>
          <Nav variant="pills" className="flex-row" id="auth-tab">
            <Nav.Item>
              <Nav.Link eventKey="login">Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="register">Register</Nav.Link>
            </Nav.Item>
          </Nav>
          <Tab.Content>
            <Tab.Pane eventKey="login">
              <Login toggleShow={toggleShow} />
            </Tab.Pane>
            <Tab.Pane eventKey="register">
              <Register toggleShow={toggleShow} />
            </Tab.Pane>
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
}

export default AuthPage;

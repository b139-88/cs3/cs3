import {
  Col, Nav, Row, Tab,
} from 'react-bootstrap';
import Profile from '../../components/user/Profile';
import Addresses from '../../components/user/Addresses';
import Orders from '../../components/core/Orders';

function MyAccount() {
  return (
    <Tab.Container defaultActiveKey="profile">
      <Row className="justify-content-center my-5 mx-5">
        <Col md={1} className="mx-5 mb-3">
          <Nav variant="pills" className="flex-column">
            <Nav.Item>
              <Nav.Link eventKey="profile">Profile</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="addresses">Addresses</Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link eventKey="orders">Orders</Nav.Link>
            </Nav.Item>
          </Nav>
        </Col>
        <Col md={7} className="mx-3">
          <Tab.Content>
            <Tab.Pane eventKey="profile">
              <Profile />
            </Tab.Pane>
            <Tab.Pane eventKey="addresses">
              <Addresses />
            </Tab.Pane>
            <Tab.Pane eventKey="orders">
              <Orders />
            </Tab.Pane>
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>
  );
}

export default MyAccount;

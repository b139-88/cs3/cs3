import axios from 'axios';
import { useContext, useEffect, useState } from 'react';
import {
  Container, Row, Col, Form, FloatingLabel, Alert, Button,
} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { API } from '../../config';
import AuthContext from '../../contexts/AuthContext';

function Profile() {
  const { user } = useContext(AuthContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [currentPassword, setCurrentPassword] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [alertStatus, setAlertStatus] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const [updateButtonStatus, setUpdateButtonStatus] = useState(false);

  const handleChangePassword = (e) => {
    e.preventDefault();

    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    const payload = {
      currentPassword,
      newPassword: password,
      newConfirmPassword: confirmPassword,
    };

    axios.patch(`${API}/users/${user.id}/password`, payload, { headers })
      .then((res) => {
        if (res.data.success) {
          setCurrentPassword('');
          setPassword('');
          setConfirmPassword('');
          Swal.fire({
            title: 'Updated!',
            text: 'Password has been updated successfully!',
            icon: 'success',
            confirmButtonColor: '#343a40',
          });
        }
      })
      .catch((err) => {
        console.log(err.response);
        setAlertStatus('danger');
        setAlertMessage(err.response.data.message);
        setShowAlert(true);
      });
  };

  useEffect(() => {
    if (currentPassword !== '' && password !== '' && confirmPassword !== '') {
      if (password !== confirmPassword) {
        setAlertStatus('danger');
        setAlertMessage('New password does not match');
        setShowAlert(true);
        setUpdateButtonStatus(true);
      } else {
        setAlertStatus('');
        setAlertMessage('');
        setShowAlert(false);
        setUpdateButtonStatus(false);
      }
    } else {
      setAlertStatus('');
      setAlertMessage('');
      setShowAlert(false);
      setUpdateButtonStatus(true);
    }
  }, [currentPassword, password, confirmPassword]);

  useEffect(() => {
    setFirstName(user.firstName);
    setLastName(user.lastName);
    setEmail(user.email);
  }, []);

  return (
    <Container>
      <Row className="justify-content-center text-center">
        <Col>
          <Form onSubmit={(e) => handleChangePassword(e)}>
            <Row>
              <Col>
                <Form.Group controlId="formFirstName" className="my-2">
                  <FloatingLabel
                    controlId="floatingFirstName"
                    label="FIrst Name"
                    className="mb-3"
                  >
                    <Form.Control
                      type="text"
                      value={firstName}
                      onChange={(e) => setFirstName(e.target.value)}
                      placeholder="First Name"
                      disabled
                    />
                  </FloatingLabel>
                </Form.Group>

              </Col>
              <Col>
                <Form.Group controlId="formLastName" className="my-2">
                  <FloatingLabel
                    controlId="floatingLastName"
                    label="Last Name"
                    className="mb-3"
                  >
                    <Form.Control
                      type="text"
                      value={lastName}
                      onChange={(e) => setLastName(e.target.value)}
                      placeholder="Last Name"
                      disabled
                    />
                  </FloatingLabel>
                </Form.Group>
              </Col>
              <Form.Group controlId="formEmail" className="my-2">
                <FloatingLabel
                  controlId="floatingEmailRegister"
                  label="Email"
                  className="mb-3"
                >
                  <Form.Control
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email"
                    disabled
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group controlId="formCurrentPassword" className="my-2">
                <FloatingLabel
                  controlId="floatingCurrentPasswordRegister"
                  label="Current Password"
                  className="mb-3"
                >
                  <Form.Control
                    type="password"
                    value={currentPassword}
                    onChange={(e) => setCurrentPassword(e.target.value)}
                    placeholder="Current Password"
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group controlId="formPassword" className="my-2">
                <FloatingLabel
                  controlId="floatingPasswordRegister"
                  label="Password"
                  className="mb-3"
                >
                  <Form.Control
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Password"
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group controlId="formConfirmPassword" className="my-2">
                <FloatingLabel
                  controlId="floatingConfirmPassword"
                  label="Confirm Password"
                  className="mb-3"
                >
                  <Form.Control
                    type="password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    placeholder="Confirm Password"
                  />
                  <Form.Text className="text-muted">
                    Enter new password if you wish to change your password.
                  </Form.Text>
                </FloatingLabel>
              </Form.Group>

            </Row>
            {
                showAlert
                  ? (
                    <Alert
                      id="alert-notificaton"
                      show={showAlert}
                      variant={alertStatus}
                      onClose={() => setShowAlert(false)}
                      dismissible
                    >
                      {alertMessage}
                    </Alert>
                  )
                  : null
            }
            <div className="d-grid gap-2">
              <Button type="submit" variant="dark" disabled={updateButtonStatus} className="my-3" size="lg">Update</Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

export default Profile;

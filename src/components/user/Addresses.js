/* eslint-disable no-use-before-define */
/* eslint-disable no-underscore-dangle */
import axios from 'axios';
import { useEffect, useState } from 'react';
import {
  Button, Container, Modal, Table,
} from 'react-bootstrap';
import { FaEdit, FaTrash } from 'react-icons/fa';
import Swal from 'sweetalert2';
import { API } from '../../config';
import AddressForm from './AddressForm';

function Addresses() {
  const [address, setAddress] = useState({});
  const [addressesTable, setAddressesTable] = useState([]);
  const [showEditModal, setShowEditModal] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const toggleShowEditModal = () => setShowEditModal((p) => !p);

  const iconStyle = {
    cursor: 'pointer',
    fontSize: '1em',
    margin: '5px',
  };

  const openEditAddress = async (addressId) => {
    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    const data = await axios.get(`${API}/users/address/${addressId}`, { headers })
      .then((res) => res.data.address)
      .catch((err) => console.error(err));

    setAddress(data);
    setIsUpdate(true);
    toggleShowEditModal();
  };

  const openAddAddress = async () => {
    setAddress({});
    setIsUpdate(false);
    toggleShowEditModal();
  };

  const deleteAddress = (addressId) => {
    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    Swal.fire({
      title: 'Are you sure you want to delete this address?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#343a40',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`${API}/users/address/${addressId}`, { headers })
          .then((res) => {
            if (res.data.success) {
              Swal.fire({
                title: 'Deleted!',
                text: 'Address has been deleted successfully!',
                icon: 'success',
                confirmButtonColor: '#343a40',
              });

              getAddresses();
            }
          })
          .catch(() => {
            Swal.fire({
              title: 'Oh no!',
              text: 'An error has occurred while deleting this address. Please try again.',
              icon: 'error',
              confirmButtonColor: '#343a40',
            });
          });
      }
    });
  };

  const getAddresses = () => {
    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    axios.get(`${API}/users/address`, { headers })
      .then((res) => {
        const addressTable = res.data.addresses.map((addr) => (
          <tr key={addr._id}>
            <td>{addr.addressType}</td>
            <td>{`${addr.firstName} ${addr.lastName}`}</td>
            <td>{`${addr.addressLine1} ${addr.addressLine2}, ${addr.city}, ${addr.state}, ${addr.zipCode}`}</td>
            <td>{addr.mobileNumber}</td>
            <td>
              <FaEdit onClick={() => openEditAddress(addr._id)} key={`upd-${addr._id}`} style={iconStyle} id="social-icon" />
              <FaTrash onClick={() => deleteAddress(addr._id)} key={`dlt-${addr._id}`} style={iconStyle} id="social-icon" />
            </td>
          </tr>
        ));
        setAddressesTable(addressTable);
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getAddresses();
  }, []);

  return (
    <Container>
      <Table responsive>
        <thead>
          <tr>
            <th>Type</th>
            <th>Name</th>
            <th>Address</th>
            <th>Contact #</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          { addressesTable }
        </tbody>
      </Table>
      <div className="d-grid gap-2">
        <Button onClick={() => openAddAddress()} variant="dark" className="my-3" size="lg">Add New Address</Button>
      </div>
      <Modal
        show={showEditModal}
        onHide={() => toggleShowEditModal()}
        size="lg"
      >
        <Modal.Header closeButton>
          {
            isUpdate
              ? <Modal.Title>Update Address</Modal.Title>
              : <Modal.Title>Add Address</Modal.Title>
          }
        </Modal.Header>
        <AddressForm
          toggleShowEditModal={toggleShowEditModal}
          getAddresses={getAddresses}
          address={address}
          isUpdate={isUpdate}
        />
      </Modal>
    </Container>

  );
}

export default Addresses;

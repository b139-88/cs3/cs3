/* eslint-disable no-underscore-dangle */
import {
  Col, Container, Form, Row, Button, FloatingLabel, Alert,
} from 'react-bootstrap';
import { useContext, useEffect, useState } from 'react';
import { FaUserLock, FaFacebook, FaGoogle } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { login, authenticate, getUserDetails } from '../../Auth';
import AuthContext from '../../contexts/AuthContext';

export default function Login(props) {
  const { setUser } = useContext(AuthContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [alertStatus, setAlertStatus] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const navigate = useNavigate();

  // eslint-disable-next-line react/prop-types
  const { toggleShow } = props;

  const headerIconStyle = {
    color: '#343a40',
    fontSize: '4em',
    margin: '20px',
  };

  const socialIconStyle = {
    color: '#343a40',
    fontSize: '1.5em',
    margin: '5px',
  };

  const handleLogin = (e) => {
    e.preventDefault();

    login({ email, password })
      .then((res) => {
        if (!res.data.success) {
          setAlertStatus('danger');
          setAlertMessage(res.data.message);
          setShowAlert(true);
          if (res.data.message === 'User does not exists.') {
            setEmail('');
          }
          setPassword('');
        } else {
          authenticate(res, () => {
            getUserDetails(localStorage.getItem('token'))
              .then((response) => {
                if (typeof response.data.user._id !== 'undefined') {
                  setUser({
                    id: response.data.user._id,
                    firstName: response.data.user.firstName,
                    lastName: response.data.user.lastName,
                    email: response.data.user.email,
                    isAdmin: response.data.user.isAdmin,
                  });
                } else {
                  Swal.fire({
                    title: 'Error occurred!',
                    icon: 'error',
                    text: 'An error occurred while trying to retrieve user details. Please try again.',
                  });
                }
              });

            setEmail('');
            setPassword('');
          });

          toggleShow();
        }
      });

    navigate(+1);
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsButtonDisabled(false);
    } else {
      setIsButtonDisabled(true);
    }
  }, [email, password]);

  return (
    <Container>
      <Row className="justify-content-center text-center">
        <Col>
          <Form onSubmit={(e) => handleLogin(e)}>
            <FaUserLock style={headerIconStyle} />
            <Form.Group controlId="formEmail" className="my-2">
              <FloatingLabel
                controlId="floatingEmailLogin"
                label="Email"
                className="mb-3"
              >
                <Form.Control
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  placeholder="name@example.com"
                />
              </FloatingLabel>
            </Form.Group>
            <Form.Group controlId="formPassword" className="my-2">
              <FloatingLabel
                controlId="floatingPasswordLogin"
                label="Password"
                className="mb-3"
              >
                <Form.Control
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  placeholder="Password"
                />
              </FloatingLabel>
            </Form.Group>
            {
                showAlert
                  ? (
                    <Alert
                      id="alert-notificaton"
                      show={showAlert}
                      variant={alertStatus}
                      onClose={() => setShowAlert(false)}
                      dismissible
                    >
                      {alertMessage}
                    </Alert>
                  )
                  : null
            }
            <div className="d-grid gap-2">
              <Button type="submit" variant="dark" className="my-3" size="lg" disabled={isButtonDisabled}>Login</Button>
            </div>
            <div className="my-2">or login using</div>
            <div className="social-container my-3 mx-3">
              <a href="/#" className="social">
                <FaFacebook style={socialIconStyle} id="social-icon" />
              </a>
              <a href="/#" className="social">
                <FaGoogle style={socialIconStyle} id="social-icon" />
              </a>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

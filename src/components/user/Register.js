/* eslint-disable no-underscore-dangle */
import {
  Col, Container, Form, Row, Button, FloatingLabel, Alert,
} from 'react-bootstrap';
import { useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { FaUserPlus, FaFacebook, FaGoogle } from 'react-icons/fa';
import Swal from 'sweetalert2';
import { register, authenticate, getUserDetails } from '../../Auth';
import AuthContext from '../../contexts/AuthContext';

export default function Register(props) {
  const { setUser } = useContext(AuthContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [alertStatus, setAlertStatus] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const navigate = useNavigate();

  // eslint-disable-next-line react/prop-types
  const { toggleShow } = props;

  const headerIconStyle = {
    color: '#444444',
    fontSize: '4em',
    margin: '20px',
  };

  const socialIconStyle = {
    color: '#444444',
    fontSize: '1.5em',
    margin: '5px',
  };

  const handleRegister = (e) => {
    e.preventDefault();

    register({
      firstName, lastName, email, password,
    })
      .then((res) => {
        if (!res.data.success) {
          setAlertStatus('danger');
          setAlertMessage(res.data.message);
          setShowAlert(true);
          if (res.data.message === 'Email already exists.') {
            setEmail('');
          }
          setPassword('');
          setConfirmPassword('');
        } else {
          authenticate(res, () => {
            getUserDetails(localStorage.getItem('token'))
              .then((response) => {
                if (typeof response.data.user._id !== 'undefined') {
                  setUser({
                    id: response.data.user._id,
                    firstName: response.data.user.firstName,
                    lastName: response.data.user.lastName,
                    email: response.data.user.email,
                    isAdmin: response.data.user.isAdmin,
                  });

                  Swal.fire({
                    title: 'Yay!',
                    icon: 'success',
                    text: 'You have successfully registered!',
                  });
                } else {
                  Swal.fire({
                    title: 'Error occurred!',
                    icon: 'error',
                    text: 'An error occurred while trying to retrieve user details. Please try again.',
                  });
                }
              });

            setEmail('');
            setPassword('');
          });

          toggleShow();
        }
      });

    navigate(+1);
  };

  return (
    <Container>
      <Row className="justify-content-center text-center">
        <Col>
          <Form onSubmit={(e) => handleRegister(e)}>
            <FaUserPlus style={headerIconStyle} />
            <Row>
              <Col>
                <Form.Group controlId="formFirstName" className="my-2">
                  <FloatingLabel
                    controlId="floatingFirstName"
                    label="FIrst Name"
                    className="mb-3"
                  >
                    <Form.Control
                      type="text"
                      value={firstName}
                      onChange={(e) => setFirstName(e.target.value)}
                      placeholder="First Name"
                    />
                  </FloatingLabel>
                </Form.Group>

              </Col>
              <Col>
                <Form.Group controlId="formLastName" className="my-2">
                  <FloatingLabel
                    controlId="floatingLastName"
                    label="Last Name"
                    className="mb-3"
                  >
                    <Form.Control
                      type="text"
                      value={lastName}
                      onChange={(e) => setLastName(e.target.value)}
                      placeholder="Last Name"
                    />
                  </FloatingLabel>
                </Form.Group>
              </Col>
              <Form.Group controlId="formEmail" className="my-2">
                <FloatingLabel
                  controlId="floatingEmailRegister"
                  label="Email"
                  className="mb-3"
                >
                  <Form.Control
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    placeholder="Email"
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group controlId="formPassword" className="my-2">
                <FloatingLabel
                  controlId="floatingPasswordRegister"
                  label="Password"
                  className="mb-3"
                >
                  <Form.Control
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    placeholder="Password"
                  />
                </FloatingLabel>
              </Form.Group>
              <Form.Group controlId="formConfirmPassword" className="my-2">
                <FloatingLabel
                  controlId="floatingConfirmPassword"
                  label="Confirm Password"
                  className="mb-3"
                >
                  <Form.Control
                    type="password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    placeholder="Confirm Password"
                  />
                </FloatingLabel>
              </Form.Group>
            </Row>
            {
                showAlert
                  ? (
                    <Alert
                      id="alert-notificaton"
                      show={showAlert}
                      variant={alertStatus}
                      onClose={() => setShowAlert(false)}
                      dismissible
                    >
                      {alertMessage}
                    </Alert>
                  )
                  : null
            }
            <div className="d-grid gap-2">
              <Button type="submit" variant="dark" className="my-3" size="lg">Register</Button>
            </div>
            <div className="my-2">or register using</div>
            <div className="social-container my-3 mx-3">
              <a href="/#" className="social">
                <FaFacebook style={socialIconStyle} id="social-icon" />
              </a>
              <a href="/#" className="social">
                <FaGoogle style={socialIconStyle} id="social-icon" />
              </a>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}

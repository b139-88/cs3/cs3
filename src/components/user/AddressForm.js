/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import axios from 'axios';
import { useEffect, useState } from 'react';
import {
  Col, Form, Row, Modal, FloatingLabel, Button, Alert,
} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { API } from '../../config';

function AddressForm(props) {
  const {
    isUpdate, address, toggleShowEditModal, getAddresses,
  } = props;

  const [addressId] = useState(isUpdate ? address._id : '');
  const [addressType, setAddressType] = useState(isUpdate ? address.addressType : '');
  const [firstName, setFirstName] = useState(isUpdate ? address.firstName : '');
  const [lastName, setLastName] = useState(isUpdate ? address.lastName : '');
  const [addressLine1, setAddressLine1] = useState(isUpdate ? address.addressLine1 : '');
  const [addressLine2, setAddressLine2] = useState(isUpdate ? address.addressLine2 : '');
  const [city, setCity] = useState(isUpdate ? address.city : '');
  const [state, setState] = useState(isUpdate ? address.state : '');
  const [zipCode, setZipCode] = useState(isUpdate ? address.zipCode : '');
  const [mobileNumber, setMobileNumber] = useState(isUpdate ? address.mobileNumber : '');
  const [isDefaultBilling, setIsDefaultBilling] = useState(isUpdate ? address.isDefaultBilling : false);
  const [isDefaultShipping, setIsDefaultShipping] = useState(isUpdate ? address.isDefaultShipping : false);
  const [showAlert, setShowAlert] = useState(false);
  const [alertStatus, setAlertStatus] = useState('');
  const [alertMessage, setAlertMessage] = useState('');
  const [buttonStatus, setButtonStatus] = useState(false);
  const toggleDefaultBilling = () => setIsDefaultBilling((p) => !p);
  const toggleDefaultShipping = () => setIsDefaultShipping((p) => !p);

  const handleUpdateAddress = (e) => {
    e.preventDefault();

    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    const payload = {
      firstName,
      lastName,
      addressLine1,
      addressLine2,
      city,
      state,
      zipCode,
      mobileNumber,
      isDefaultBilling,
      isDefaultShipping,
      addressType,
    };

    axios.put(`${API}/users/address/${addressId}`, payload, { headers })
      .then((res) => {
        if (res.data.success) {
          Swal.fire({
            title: 'Updated!',
            text: 'Address has been deleted successfully!',
            icon: 'success',
            confirmButtonColor: '#343a40',
          });

          toggleShowEditModal();
          getAddresses();
        }
      })
      .catch((err) => {
        setAlertStatus('danger');
        setAlertMessage(err.response.data);
        setShowAlert(true);
      });
  };

  const handleAddAddress = (e) => {
    e.preventDefault();

    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    const payload = {
      firstName,
      lastName,
      addressLine1,
      addressLine2,
      city,
      state,
      zipCode,
      mobileNumber,
      isDefaultBilling,
      isDefaultShipping,
      addressType,
    };

    axios.post(`${API}/users/address/${addressId}`, payload, { headers })
      .then((res) => {
        if (res.data.success) {
          Swal.fire({
            title: 'Updated!',
            text: 'Address has been added successfully!',
            icon: 'success',
            confirmButtonColor: '#343a40',
          });

          toggleShowEditModal();
          getAddresses();
        }
      })
      .catch((err) => {
        setAlertStatus('danger');
        setAlertMessage(err.response.data.message);
        setShowAlert(true);
      });
  };

  useEffect(() => {
    if (firstName !== '' && lastName !== '' && addressLine1 !== '' && addressLine2 !== '' && city !== '' && state !== '' && zipCode !== '' && mobileNumber !== '' && addressType !== '') {
      setButtonStatus(false);
    } else {
      setButtonStatus(true);
    }
  }, [firstName, lastName, addressLine1, addressLine2, city, state, zipCode, mobileNumber, addressType]);

  return (
    <Form onSubmit={isUpdate ? (e) => handleUpdateAddress(e) : (e) => handleAddAddress(e)}>
      <Modal.Body>

        <Row>
          <Col>
            <Form.Group controlId="formFirstName" className="my-2">
              <FloatingLabel
                controlId="floatingFirstName"
                label="First Name"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={firstName}
                  onChange={(e) => setFirstName(e.target.value)}
                  placeholder="First Name"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
          <Col>
            <Form.Group controlId="formLastName" className="my-2">
              <FloatingLabel
                controlId="floatingLastName"
                label="Last Name"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                  placeholder="Last Name"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="formAddressLine1" className="my-2">
              <FloatingLabel
                controlId="floatingAddressLine1"
                label="Address Line 1"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={addressLine1}
                  onChange={(e) => setAddressLine1(e.target.value)}
                  placeholder="Address Line 1"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="formAddressLine2" className="my-2">
              <FloatingLabel
                controlId="floatingAddressLine2"
                label="Address Line 2"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={addressLine2}
                  onChange={(e) => setAddressLine2(e.target.value)}
                  placeholder="Address Line 2"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="formCity" className="my-2">
              <FloatingLabel
                controlId="floatingCity"
                label="City"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={city}
                  onChange={(e) => setCity(e.target.value)}
                  placeholder="City"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
          <Col>
            <Form.Group controlId="formState" className="my-2">
              <FloatingLabel
                controlId="floatingState"
                label="State"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={state}
                  onChange={(e) => setState(e.target.value)}
                  placeholder="State"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
          <Col>
            <Form.Group controlId="formZipCode" className="my-2">
              <FloatingLabel
                controlId="floatingZipCode"
                label="Zip Code"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={zipCode}
                  onChange={(e) => setZipCode(e.target.value)}
                  placeholder="Zip Code"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col>
            <Form.Group controlId="formMobileNumber" className="my-2">
              <FloatingLabel
                controlId="floatingMobileNumber"
                label="Mobile Number"
                className="mb-3"
              >
                <Form.Control
                  type="text"
                  value={mobileNumber}
                  onChange={(e) => setMobileNumber(e.target.value)}
                  placeholder="Mobile Number"
                />
              </FloatingLabel>
            </Form.Group>
          </Col>
          <Col>
            <Form.Group controlId="formAddressType" className="my-2">
              <FloatingLabel
                controlId="floatingAddressType"
                label="Address Type"
                className="mb-3"
              >
                <Form.Select aria-label="Home" value={addressType} onChange={(e) => (e.target.value !== 'Select' ? setAddressType(e.target.value) : setAddressType(''))}>
                  <option value="Select">Select address type</option>
                  <option value="Home">Home</option>
                  <option value="Office">Office</option>
                </Form.Select>
              </FloatingLabel>
            </Form.Group>
          </Col>
          <Col className="mt-2">
            <Form.Check
              type="switch"
              id="isDefaultBilling"
              label="Default Billing"
              checked={isDefaultBilling}
              onChange={() => toggleDefaultBilling()}
            />
            <Form.Check
              type="switch"
              id="isDefaultShipping"
              label="Default Shipping"
              checked={isDefaultShipping}
              onChange={() => toggleDefaultShipping()}
            />
          </Col>
          {
                showAlert
                  ? (
                    <Alert
                      id="alert-notificaton"
                      show={showAlert}
                      variant={alertStatus}
                      onClose={() => setShowAlert(false)}
                      dismissible
                    >
                      {alertMessage}
                    </Alert>
                  )
                  : null
            }
        </Row>
      </Modal.Body>
      <Modal.Footer>
        <Button type="submit" disabled={buttonStatus} variant="primary">Submit</Button>
      </Modal.Footer>
    </Form>
  );
}

export default AddressForm;

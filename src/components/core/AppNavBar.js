import { useContext, useEffect, useState } from 'react';
import {
  Container, Navbar, Nav, Modal, NavDropdown, Button,
} from 'react-bootstrap';
import { FaUserAlt, FaShoppingCart } from 'react-icons/fa';
import { useNavigate } from 'react-router-dom';
import Login from '../user/Login';
import Register from '../user/Register';
import AuthContext from '../../contexts/AuthContext';

function AppNavbar() {
  const navigate = useNavigate();
  const { user, unsetUser } = useContext(AuthContext);
  const [showLogin, setShowLogin] = useState(false);
  const [showRegister, setShowRegister] = useState(false);
  const toggleShowLogin = () => setShowLogin((p) => !p);
  const toggleShowRegister = () => setShowRegister((p) => !p);

  const logout = () => {
    unsetUser();
    navigate('/');
  };

  const loginNavConditionWrapper = () => (
    user.id != null
      ? (
        <NavDropdown title={<FaUserAlt />} className="active" id="collapsible-nav-dropdown">
          <NavDropdown.Item href="/my-account">My account</NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
        </NavDropdown>
      )
      : (
        <>
          <Button variant="light" onClick={() => toggleShowLogin()}>Login</Button>
          <Button onClick={() => toggleShowRegister()}>Register</Button>
        </>
      )
  );

  useEffect(() => {
    loginNavConditionWrapper();
  }, [user]);

  return (
    <Navbar collapseOnSelect expand="lg" bg="light" variant="light">
      <Container>
        <Navbar.Brand href="/">Foliage</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav activeKey={window.location.pathname} className="m-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/products">Products</Nav.Link>
            <Nav.Link href="/contact-us">Contact Us</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link href="/cart"><FaShoppingCart /></Nav.Link>
            { loginNavConditionWrapper() }
          </Nav>
        </Navbar.Collapse>
        <Modal
          show={showLogin}
          onHide={() => toggleShowLogin()}
          aria-labelledby="auth-modal"
          centered
        >
          <Modal.Body>
            <Container>
              <Login toggleShow={toggleShowLogin} />
            </Container>
          </Modal.Body>
        </Modal>
        <Modal
          show={showRegister}
          onHide={() => toggleShowRegister()}
          aria-labelledby="auth-modal"
          centered
        >
          <Modal.Body>
            <Container>
              <Register toggleShow={toggleShowRegister} />
            </Container>
          </Modal.Body>
        </Modal>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;

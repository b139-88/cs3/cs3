/* eslint-disable react/prop-types */
import { Col, Container, Row } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { useNavigate } from 'react-router-dom';

export default function Error({ errorProps }) {
  const navigate = useNavigate();

  function goToUrl() {
    navigate(-1);
  }

  return (
    <Container>
      <Row className="justify-content-center text-center my-5 py-5">
        <Col>
          <h1>{ errorProps.title }</h1>
          <p>{ errorProps.message }</p>
          <Button onClick={() => goToUrl()}>Go back</Button>
        </Col>
      </Row>
    </Container>

  );
}

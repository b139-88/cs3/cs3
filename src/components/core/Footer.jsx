import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import {
  FaFacebook, FaInstagram, FaTwitter, FaMapMarkerAlt, FaPhoneAlt, FaEnvelope,
} from 'react-icons/fa';
import Newsletter from './Newsletter';

function Footer() {
  const socialIconStyle = {
    fontSize: '1.5em',
    margin: '5px',
  };

  return (
    <Container fluid id="footer-container" className="p-0 m-0 mt-auto">

      <Container className="mb-5">
        <Newsletter />
        <Row>
          <Col id="footer-left" className="p-3" sm={12} md={4}>
            <h4>Foliage</h4>
            <p>
              Lush foliage, tiny succulents, cacti and sansevieria galore.
              Browse our shop and leave with a new friend or two.
              The charm lies in the unique selection of each beautiful storefront.
              All of these will help fill your home with an abundance of greenery.
            </p>
            <div className="social-icons">
              <FaFacebook style={socialIconStyle} />
              <FaInstagram style={socialIconStyle} />
              <FaTwitter style={socialIconStyle} />
            </div>
          </Col>
          <Col id="footer-center" className="p-3" sm={12} md={4}>
            <h4>Useful Links</h4>
            <ul>
              <li><a href="/">Home</a></li>
              <li><a href="/my-account">My Account</a></li>
              <li><a href="/cart">Cart</a></li>
              <li><a href="/#">Wishlist</a></li>
              <li><a href="/products">Products</a></li>
              <li><a href="/#">About Us</a></li>
              <li><a href="/#">Terms and Conditions</a></li>
              <li><a href="/#">Privacy Policy</a></li>
            </ul>
          </Col>
          <Col id="footer-right" className="p-3" sm={12} md={4}>
            <h4>Contact Us</h4>
            <p>
              <FaMapMarkerAlt />
              123 Pasco Ave., Brgy. Santolan, Pasig City
            </p>
            <p>
              <FaPhoneAlt />
              +63 906 123 4567

            </p>
            <p>
              <FaEnvelope />
              hello@foliage.ph
            </p>
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

export default Footer;

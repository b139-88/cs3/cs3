/* eslint-disable no-underscore-dangle */
import axios from 'axios';
import { useEffect, useState } from 'react';
import {
  Button, Card, Col, Container, Modal, Row, Table,
} from 'react-bootstrap';
import { API } from '../../config';

function Orders() {
  const [orders, setOrders] = useState([]);
  const [viewOrder, setViewOrder] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const toggleShowModal = () => setShowModal((p) => !p);

  const handleViewOrder = (e, orderId) => {
    e.preventDefault();
    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    axios.get(`${API}/users/orders/${orderId}`, { headers })
      .then((res) => {
        const { order } = res.data;
        const odr = (
          <>
            <Modal.Header closeButton>
              <Modal.Title>Order</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <Row>
                <Col className="mb-2" xs={12} lg={4}>
                  <Card>
                    <Card.Header>Order Details</Card.Header>
                    <Card.Body>
                      <Card.Text className="mb-0">Order ID:</Card.Text>
                      <Card.Subtitle className="mb-2">{orderId}</Card.Subtitle>
                      <Card.Text className="mb-0">Order Date:</Card.Text>
                      <Card.Subtitle className="mb-2">{order.createdAt}</Card.Subtitle>
                      <Card.Text className="mb-0">Payment Method:</Card.Text>
                      <Card.Subtitle className="mb-2">{order.paymentMethod}</Card.Subtitle>
                      <Card.Text className="mb-0">Shipping Method:</Card.Text>
                      <Card.Subtitle className="mb-2">{order.shippingMethod}</Card.Subtitle>
                    </Card.Body>
                  </Card>
                </Col>
                <Col className="mb-2" xs={12} md={6} lg={4}>
                  <Card>
                    <Card.Header>Billing Details</Card.Header>
                    <Card.Body>
                      <Card.Text className="mb-0">Name:</Card.Text>
                      <Card.Subtitle className="mb-2">{`${order.billingAddress.firstName} ${order.billingAddress.lastName}`}</Card.Subtitle>
                      <Card.Text className="mb-0">Address:</Card.Text>
                      <Card.Subtitle className="mb-2">{`${order.billingAddress.addressLine1} ${order.billingAddress.addressLine2}, ${order.billingAddress.city}, ${order.billingAddress.state}, ${order.billingAddress.zipCode}`}</Card.Subtitle>
                      <Card.Text className="mb-0">Contact #:</Card.Text>
                      <Card.Subtitle className="mb-2">{order.billingAddress.mobileNumber}</Card.Subtitle>
                    </Card.Body>
                  </Card>
                </Col>
                <Col className="mb-2" xs={12} md={6} lg={4}>
                  <Card>
                    <Card.Header>Shipping Details</Card.Header>
                    <Card.Body>
                      <Card.Text className="mb-0">Name:</Card.Text>
                      <Card.Subtitle className="mb-2">{`${order.shippingAddress.firstName} ${order.shippingAddress.lastName}`}</Card.Subtitle>
                      <Card.Text className="mb-0">Address:</Card.Text>
                      <Card.Subtitle className="mb-2">{`${order.shippingAddress.addressLine1} ${order.shippingAddress.addressLine2}, ${order.shippingAddress.city}, ${order.shippingAddress.state}, ${order.shippingAddress.zipCode}`}</Card.Subtitle>
                      <Card.Text className="mb-0">Contact #:</Card.Text>
                      <Card.Subtitle className="mb-2">{order.shippingAddress.mobileNumber}</Card.Subtitle>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Card>
                    <Card.Header>Order Items</Card.Header>
                    <Card.Body>
                      <Table responsive className="text-center">
                        <thead>
                          <tr>
                            <th>Item Name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>Subtotal</th>
                          </tr>
                        </thead>
                        <tbody>
                          {order.products.map((product) => (
                            <tr key={product.productId._id}>
                              <td>{product.productId.productName}</td>
                              <td>{product.quantity}</td>
                              <td>{`₱ ${product.productId.price.toLocaleString(undefined, { minimumFractionDigits: 2 })}`}</td>
                              <td>{`₱ ${product.cartLineItemTotal.toLocaleString(undefined, { minimumFractionDigits: 2 })}`}</td>
                            </tr>
                          ))}
                        </tbody>
                      </Table>
                    </Card.Body>
                  </Card>
                </Col>
              </Row>
              <Row>
                <Col>
                  <div className="mt-3">
                    <span className="badge bg-dark">Shipping Cost:</span>
                    <span className="mx-2">
                      ₱
                      {order.shippingCost.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                    </span>
                  </div>
                  <div className="mt-2">
                    <span className="badge bg-dark">Total Cost:</span>
                    <span className="mx-2">
                      ₱
                      {order.orderTotal.toLocaleString(undefined, { minimumFractionDigits: 2 })}
                    </span>
                  </div>
                </Col>
              </Row>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => toggleShowModal()}>Close</Button>
            </Modal.Footer>
          </>
        );

        setViewOrder(odr);
      });
    toggleShowModal();
  };

  const getOrders = () => {
    const headers = {
      'content-type': 'application/json',
      authorization: `Bearer ${localStorage.getItem('token')}`,
    };

    axios.get(`${API}/users/orders/`, { headers })
      .then((res) => {
        if (res.data.success) {
          const ordersArr = res.data.orders.map((order) => (
            <tr key={order._id}>
              <td>{order.createdAt}</td>
              <td>
                <a href="/#" onClick={(e) => handleViewOrder(e, order._id)}>{order._id}</a>
              </td>
              <td>{order.status}</td>
            </tr>
          ));
          setOrders(ordersArr);
        }
      });
  };

  useEffect(() => {
    getOrders();
  }, []);

  return (
    <Container>
      <Table responsive>
        <thead>
          <tr>
            <th>Order Date</th>
            <th>Order ID</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          { orders }
        </tbody>
      </Table>
      <Modal
        show={showModal}
        onHide={() => toggleShowModal()}
        size="xl"
      >
        {viewOrder}
      </Modal>
    </Container>
  );
}

export default Orders;

import { useState } from 'react';
import {
  Col, Row, Container, Button, InputGroup, FormControl,
} from 'react-bootstrap';

function Newsletter() {
  const [email, setEmail] = useState('');

  return (
    <Container fluid id="footer-newsletter" className="my-4">
      <Row className="justify-content-center text-center">
        <Col>
          <h1 className="mt-5">Newsletter</h1>
          <p>Get exclusive discounts and promotions!</p>
        </Col>
      </Row>
      <Row>
        <Col xs={{ span: 4, offset: 4 }}>
          <InputGroup className="m-0 p-0 mb-5">
            <FormControl
              placeholder="Email"
              aria-label="Email"
              aria-describedby="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <Button id="email">
              Subscribe
            </Button>
          </InputGroup>
        </Col>
      </Row>
    </Container>
  );
}

export default Newsletter;

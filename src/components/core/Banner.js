import { Container, Row, Col } from 'react-bootstrap';
import Image from 'react-bootstrap/Image';

function Banner() {
  return (
    <Container className="mb-5 align-content-center" id="top-banner-container">
      <Row>
        <Col className="mt-2 pr-0" sm={12} md={6}>
          <div className="img-container">
            <Image src="banner1.jpg" fluid id="top-banner1" />
          </div>
        </Col>
        <Col id="banner-stack" sm={12} md={6}>
          <Row className="my-2  p-0">
            <Image src="banner2.jpg" fluid id="top-banner2" />
          </Row>
          <Row className="  p-0">
            <Image src="banner3.jpg" fluid id="top-banner3" />
          </Row>
        </Col>
      </Row>
    </Container>
  );
}

export default Banner;
